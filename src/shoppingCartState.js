// shopping cart state
import _ from 'lodash'

export default {
  data: {
    cart: [
      {id: 1, title: 'Name of Product 1', price: 40, image: 'product1.jpg', qty: 1}
    ]
  },
  addProduct (product) {
    var found = _.find(this.data.cart, ['id', product.id])
    if(typeof found != 'object') {
      this.data.cart.push({
        id: product.id,
        title: product.title,
        price: product.price,
        image: product.image,
        qty: 1
      })
    }
  },
  increment (product) {
    var found = _.find(this.data.cart, ['id', product.id])
    if(typeof found == 'object') {
      var index = _.indexOf(this.data.cart, found)
      this.data.cart[index].qty++
    }
  },
  decrement (product) {
    var found = _.find(this.data.cart, ['id', product.id])
    if(typeof found == 'object') {
      var index = _.indexOf(this.data.cart, found)

      if(this.data.cart[index].qty == 1) {
        this.data.cart.$remove(found)
      } else {
        this.data.cart[index].qty--
      }
    }
  },
  incrementInCart (item) {
    var found = _.find(this.data.cart, ['id', item.id])
    if(typeof found == 'object') {
      var index = _.indexOf(this.data.cart, found)
      this.data.cart[index].qty++
    }
  },
  decrementInCart (item) {
    var found = _.find(this.data.cart, ['id', item.id])
    if(typeof found == 'object') {
      var index = _.indexOf(this.data.cart, found)

      if(this.data.cart[index].qty == 1) {
        this.data.cart.$remove(found)
      } else {
        this.data.cart[index].qty--
      }
    }
  },
  remove (item) {
    var found = _.find(this.data.cart, ['id', item.id])
    if(typeof found == 'object') {
      var index = _.indexOf(this.data.cart, found)
      this.data.cart[index].qty = 0;
      this.data.cart.$remove(found)

    }
  }
 }