# Shopping Cart
This is a task for [E-motion](https://e-motionagency.com) Company

### Tech

I use a number of open source projects to work properly:

* [Vue.js](https://vuejs.org/) - The Progressive JavaScript Framework
* [Lodash](lodash.com) - A modern JavaScript utility library delivering modularity, performance & extras.


### Installation

This task requires [Node.js](https://nodejs.org/) v4+ to run.

Install the dependencies  and start the server.

```sh
$ npm install 
$ npm run dev
```


Verify the deployment by navigating to your server address in your preferred browser.

```sh
127.0.0.1:8080
```


License
----
MIT
